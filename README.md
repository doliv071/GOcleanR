
# GOCleanR

<!-- badges: start -->
<!-- badges: end -->

The goal of GOCleanR is to ...

## Installation

You can install the development version of GOCleanR with:

``` r
devtools::install_gitlab("doliv071/GOcleanR") 
```

## Example

A basic usage example:

``` r
library(GOCleanR)
```

### Obtaining Gene Ontology Terms

```r
GO.Terms <- getTermIndex(file = "http://purl.obolibrary.org/obo/go/go-basic.obo", 
                            extract.tags = "minimal", 
                            propagate.relationships = c("is_a", "part_of"), 
                            trim.obsolete = T)
```
                            
### Obtaining Gene Ontology Term Annotations

```r
GO.Anno <- data.table::fread(input = "ftp://ftp.ebi.ac.uk/pub/databases/GO/goa/HUMAN/goa_human.gaf.gz", 
                             header = F, skip = 12, sep = "\t")
```

### Filter the Term Index 

```r
# CC = GO:0005575, MF = GO:0003674, BP = GO:0008150
GO.BP.Terms <- trimTermIndex(term.index = GO.Terms, keep.roots = "GO:0008150")
```

From here, every step of the processing pipeline will append the results to 
the term.index object. 

### Annotate The Term Index

```r
# this step can leverage multicore processes quite efficiently
GO.BP.Terms <- annotateTermIndex(term.index = GO.BP.Terms, annotation = GO.Anno, trim.dead = T, cores = 4)
```

### Calculate Term Frequency and Similarity Statistics

```r
GO.BP.Terms <- calcTermFrequency(term.index = GO.BP.Terms, cores = 16, overwrite = T)
GO.BP.Terms <- calcSimStats(term.index = GO.BP.Terms)
```

### Test a Set of Genes for GO Enrichment

```r
# load varialbe names clusters into environment
load(system.file("extdata", "clusters.RData", package = "GOCleanR"))
Test.Genes <- dplyr::filter(clusters, Cluster == 1) %>% .$Gene_Symbol %>% toupper
GO.BP.Terms <- testEnrichment(term.index = GO.BP.Terms, genes.of.interest = Test.Genes)
```

### Build the Three Main Graphs

```r
GO.BP.Terms <- buildTermGeneGraph(term.index = GO.BP.Terms, sig.val = 0.01)
GO.BP.Terms <- buildTermTermGraph(term.index = GO.BP.Terms, min.sim = 0.45, graph.sig.val = 0.01, sim.cores = 16)
GO.BP.Terms <- contractGraph(term.index = GO.BP.Terms, method = "cluster_louvain")
```

### Plot the results and Explore Their Meaning...







